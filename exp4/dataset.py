import json
import torch
import numpy as np
import util


class TrainData:
    def __init__(self, src_path, wv_path, length=75):
        with open(src_path) as f:
            self.lines = f.read().split()
            self.lines = [line.split(' ') for line in self.lines]
        self.wv = util.load_wv(wv_path)
        self.length = length

        n_line = len(self.lines)
        self.a_indices = torch.randperm(n_line - 10)
        self.p_indices = self.a_indices + torch.randint(1, 5, (n_line - 10, )).long()
        self.n_indices = torch.randperm(n_line - 1)

    def __len__(self):
        return len(self.lines) // 5

    def __getitem__(self, idx):
        a_idx = self.a_indices[idx]
        p_idx = self.p_indices[idx]
        n_idx = self.n_indices[idx]

        a_ebd = util.tokens2ebd(self.wv, self.lines[a_idx], self.length)
        p_ebd = util.tokens2ebd(self.wv, self.lines[p_idx], self.length)
        n_ebd = util.tokens2ebd(self.wv, self.lines[n_idx], self.length)

        return a_ebd, p_ebd, n_ebd


class ValidData:
    def __init__(self, src_path, wv_path, length=75):
        with open(src_path) as f:
            self.data = json.load(f)
        self.wv = util.load_wv(wv_path)
        self.length = length

        self.ebds = []
        self.ids = []
        self.tag = []
        for id_, item in enumerate(self.data):
            for line in item['que']:
                ebd = util.tokens2ebd(self.wv, line, self.length)
                self.ebds.append(ebd)
                self.ids.append(id_)
                self.tag.append('que')
            for line in item['opt']:
                ebd = util.tokens2ebd(self.wv, line, self.length)
                self.ebds.append(ebd)
                self.ids.append(id_)
                self.tag.append('opt')

    def __len__(self):
        return len(self.data)

    def get_ebds(self):
        return torch.stack(self.ebds, dim=0)

    def match(self, vecs):
        items = [{
            'que': [],
            'opt': [],
            'pred': -1,
        } for i in range(len(self.data))]

        for i, vec in enumerate(vecs):
            id_ = self.ids[i]
            tag = self.tag[i]
            items[id_][tag].append(vec)

        for item in items:
            n_que = len(item['que'])
            n_opt = len(item['opt'])
            dis = np.zeros((n_que, n_opt))
            for i in range(n_que):
                for j in range(n_opt):
                    vec1 = item['que'][i]
                    vec2 = item['opt'][j]
                    dis[i, j] = (vec1 - vec2).norm(p=2).item()
            item['pred'] = np.argmin(dis.min(axis=0))

        return [item['pred'] for item in items]

    def accuracy(self, pred):
        p = np.int32(pred)
        y = np.int32([item['ans'] for item in self.data])
        return np.mean(p == y)


if __name__ == '__main__':
    # ds = TrainData('./data/train.seg', './data/wv.pkl', 75)
    # print(len(ds))
    # a_ebd, p_ebd, n_ebd = ds[42]
    # print(a_ebd.size())
    # print(p_ebd.size())
    # print(n_ebd.size())

    ds = ValidData('./data/valid.seg', './data/wv.pkl', 75)
    ebds = ds.get_ebds()
    print(ebds.size())
    vecs = torch.rand(len(ebds), 100)
    pred = ds.match(vecs)
    acc = ds.accuracy(pred)
    print(acc)
