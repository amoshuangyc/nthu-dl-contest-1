import torch
from torch import nn
from torch.nn import functional as F

class Conv(nn.Module):
    def __init__(self, cin, cout):
        super().__init__()
        self.conv = nn.Conv1d(cin, cout, 3)
        self.bn = nn.BatchNorm1d(cout)
        self.act = nn.ReLU()

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.act(x)
        return x


class Model(nn.Module):
    def __init__(self):
        super().__init__()
        self.layers = nn.Sequential(
            Conv(250, 256),
            nn.MaxPool1d(3),
            Conv(256, 256),
            nn.MaxPool1d(3),
            Conv(256, 256),
            nn.AdaptiveMaxPool1d(1),
            nn.Conv1d(256, 256, 1),
            nn.BatchNorm1d(256),
            nn.Sigmoid()
        )

    def forward(self, x):
        return self.layers(x)


if __name__ == '__main__':
    model = Model()
    inp = torch.rand(256, 250, 45)
    out = model(inp)
    print(out.size())
