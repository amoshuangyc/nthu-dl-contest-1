import json
from pathlib import Path

import matplotlib.pyplot as plt
plt.style.use('seaborn')
import pandas as pd
from tqdm import tqdm

import torch
from torch import nn
from torch.utils.data import DataLoader

import util
from dataset import TrainData, ValidData
from model import Model

class Trainer:
    def __init__(self, exp_dir, device='cuda', n_epoch=30):
        self.exp_dir = Path(exp_dir)
        self.exp_dir.mkdir(parents=True)
        self.device = device
        self.model = Model().to(device)
        self.loss_fn = nn.TripletMarginLoss()
        self.n_epoch = n_epoch

    def fit(self, train_set, valid_set):
        self.valid_set = valid_set
        self.train_loader = DataLoader(train_set, batch_size=2048, shuffle=True, num_workers=6)

        parameters = self.model.parameters()
        self.optim = torch.optim.Adam(parameters, lr=1e-1)

        for self.epoch in range(self.n_epoch):
            self.epoch_dir = self.exp_dir / f'{self.epoch:03d}'
            self.epoch_dir.mkdir()

            print('Epoch ', self.epoch)
            with tqdm(total=len(train_set), desc='  Train', ascii=True) as pbar:
                train_metrics = self._train(pbar)
            with torch.no_grad():
                with tqdm(total=len(valid_set), desc='  Valid', ascii=True) as pbar:
                    valid_metrics = self._valid(pbar)
                self._log(train_metrics, valid_metrics)

    def _train(self, pbar):
        self.model.train()
        metrics = {
            'loss': util.RunningLoss(),
        }
        for a_batch, p_batch, n_batch in iter(self.train_loader):
            a_batch = a_batch.to(self.device)
            p_batch = p_batch.to(self.device)
            n_batch = n_batch.to(self.device)

            self.optim.zero_grad()
            a_ebd_batch = self.model(a_batch).squeeze(2)
            p_ebd_batch = self.model(p_batch).squeeze(2)
            n_ebd_batch = self.model(n_batch).squeeze(2)
            loss = self.loss_fn(a_ebd_batch, p_ebd_batch, n_ebd_batch)
            loss.backward()
            self.optim.step()

            metrics['loss'].update(loss.detach().item())
            pbar.set_postfix(metrics)
            pbar.update(a_batch.size(0))
        return metrics

    def _valid(self, pbar):
        self.model.eval()
        metrics = {
            'acc': 0.0
        }
        ebds = self.valid_set.get_ebds().to(self.device)
        vecs = self.model(ebds).cpu()
        pred = self.valid_set.match(vecs)
        metrics['acc'] = self.valid_set.accuracy(pred)
        pbar.set_postfix(metrics)
        pbar.update(len(self.valid_set))
        return metrics

    def _log(self, train_metrics, valid_metrics):
        json_path = self.exp_dir / 'log.json'
        if json_path.exists():
            df = pd.read_json(json_path)
        else:
            df = pd.DataFrame()

        metrics = {'epoch': self.epoch, **train_metrics, **valid_metrics}
        df = df.append(metrics, ignore_index=True)
        df = df.astype('str').astype('float')
        with json_path.open('w') as f:
            json.dump(df.to_dict(orient='records'), f, indent=2)

        fig, ax = plt.subplots(figsize=(15, 8), dpi=100)
        df[['loss']].plot(kind='line', ax=ax)
        fig.tight_layout()
        fig.savefig(self.exp_dir / 'loss.svg')
        plt.close()

        fig, ax = plt.subplots(figsize=(15, 8), dpi=100)
        df[['acc']].plot(kind='line', ax=ax)
        fig.tight_layout()
        fig.savefig(self.exp_dir / 'acc.svg')
        plt.close()

        if df['acc'].idxmax() == self.epoch:
            torch.save(self.model.state_dict(), self.exp_dir / 'model.pkl')


if __name__ == '__main__':
    from datetime import datetime

    exp_dir = './exp/{:%Y.%m.%d-%H:%M:%S}'.format(datetime.now())
    trainer = Trainer(exp_dir)
    train_set = TrainData('./data/train.seg', './data/wv.pkl', 40)
    valid_set = ValidData('./data/valid.seg', './data/wv.pkl', 40)
    trainer.fit(train_set, valid_set)
