import json
import numpy as np
import pandas as pd
from scipy.spatial.distance import cosine
from sklearn.metrics.pairwise import cosine_similarity
from pprint import pprint

import util

wv = util.load_wv('./data/wv.pkl')

stop_words = '\n我他們的啦了…。，「」 '

def dis_mat(A, B):
    return -cosine_similarity(A, B)

def predict(records):
    for item in records:
        dis = np.zeros((len(item['opt']), len(item['que'])))
        for i, o in enumerate(item['opt']):
            for j, q in enumerate(item['que']):
                dis[i, j] = wv.wmdistance(o, q)
        pred = np.argmin(dis.min(axis=1))
        item['pred'] = pred
    return records

with open('./data/valid.seg') as f:
    valid = json.load(f)
valid = predict(valid)
acc = np.mean(np.float32([item['pred'] == item['ans'] for item in valid]))
print(acc)

        


