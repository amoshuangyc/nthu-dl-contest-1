import json
import pandas as pd
from tqdm import tqdm
from pathlib import Path
from gensim.models import word2vec
from itertools import chain
import jieba
import util

Path('./data/').mkdir(parents=True, exist_ok=True)

# load all lines
programs = Path('../raw/').glob('Program*.csv')
lines = []
for path in programs:
    with path.open() as f:
        lines.extend(f.read()[9:-2].split('\n'))

# text segementation
jieba.initialize()
data = []
for line in tqdm(lines, desc='Segmentation', ascii=True):
    tokens = util.cut_line(line)
    data.append(tokens)
del lines

# train wv and dump
print('Word2Vec Training', end='... ', flush=True)
st0 = [list(chain.from_iterable(tokens)) for tokens in data]
st1 = data
st2 = util.merge_data(data, 2)
st4 = util.merge_data(data, 4)
st = st0 + st1 + st2 + st4
wv = word2vec.Word2Vec(
    sentences=st, size=250, window=15, min_count=1, workers=3)
wv.build_vocab([['<pad>']], update=True)
wv.save('./data/wv.pkl')
print('Done')

for s in ['貓', '一月', '可愛', '<pad>']:
    print(f'Most Similar of {s}:', wv.wv.most_similar(s))

# dump train
print('Dumping Train', end='... ', flush=True)
with open('./data/train.seg', 'w') as f:
    for tokens in data:
        f.write(' '.join(tokens))
        f.write('\n')
print('Done')

# parse and dump valid
print('Processing Valid', end='... ', flush=True)
with open('../raw/subset.json') as f:
    valid = json.load(f)
result = []
for item in valid:
    que = item['Question'].split('\n')
    que = [util.cut_line(line) for line in que if len(line)]
    opt = [item[f'Option{i}'] for i in range(6)]
    opt = [util.cut_line(line) for line in opt]
    result.append({
        'que': que,
        'opt': opt,
        'ans': item['answer']
    })
with open('./data/valid.seg', 'w') as f:
    json.dump(result, f, ensure_ascii=False, indent=2)
print('Done')

# parse and dump test
print('Processing Test', end='... ', flush=True)
test = pd.read_csv('../raw/Question.csv').to_dict(orient='record')
result = []
for item in test:
    que = item['Question'].split('\n')
    que = [util.cut_line(line) for line in que if len(line)]
    opt = [item[f'Option{i}'] for i in range(6)]
    opt = [util.cut_line(line) for line in opt]
    result.append({
        'que': que,
        'opt': opt,
    })
with open('./data/test.seg', 'w') as f:
    json.dump(result, f, ensure_ascii=False, indent=2)
print('Done')