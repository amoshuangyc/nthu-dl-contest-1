import jieba
jieba.set_dictionary('../raw/big5_dict.txt')

import torch
import numpy as np
from gensim.models import word2vec

from itertools import chain

PUNCTUATIONS = r'！？，。「」：；,.!"?…'


def merge_data(data, n):
    return [
        list(chain.from_iterable(data[i:i + n]))
        for i in range(0, len(data), n)
    ]


def is_chinese(s):
    return all('\u4e00' <= c <= '\u9fff' for c in s)


def cut_line(line):
    line = line.replace('\n', '')
    tokens = jieba.cut(line)
    tokens = [t for t in tokens if len(t.strip()) > 0]
    tokens = [t for t in tokens if is_chinese(t)]
    tokens = [t for t in tokens if t not in PUNCTUATIONS]
    return tokens


def load_wv(pkl_path):
    return word2vec.Word2Vec.load(pkl_path)


def tokens2ebd(wv, tokens, length=None, mode='torch'):
    ebds = []
    for token in tokens:
        if token in wv:
            ebds.append(wv[token])
        else:
            ebds.append(wv['<pad>'])

    if length:
        while len(ebds) < length:
            ebds.append(wv['<pad>'])
        if len(ebds) > length:
            ebds = ebds[:length]

    ebd = np.stack(ebds, axis=0)
    if mode == 'torch':
        ebd = torch.from_numpy(ebd).t().float()
    return ebd


class RunningLoss:
    def __init__(self):
        self.iter = 0
        self.avg = 0.0

    def update(self, x):
        self.avg = (self.avg * self.iter + x) / (self.iter + 1)
        self.iter += 1

    def __str__(self):
        if self.iter == 0:
            return 'nan'
        return f'{self.avg:.4f}'
