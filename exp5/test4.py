import json
from pprint import pprint

import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import paired_euclidean_distances

import util

stop_words = '\n我你他們的啦了啊喔是…。，「」 '

print('Building TFIDF', end='...', flush=True)
with open('./data/train.seg') as f:
    corpus = f.read().split('\n')
tfidf = TfidfVectorizer().fit(corpus)
vocab = tfidf.vocabulary_
idf = tfidf.idf_
print('Done')

wv = util.load_wv('./data/wv.pkl')

def sentence_embedding(words):
    words = [w for w in words if w not in stop_words]
    word_ebds = util.tokens2ebd(wv, words, mode='np')
    weights = np.zeros(len(words)) + 1e-6
    for i, word in enumerate(words):
        if word in vocab:
            weights[i] = idf[vocab[word]]
    # print(words)
    # print(weights)
    # input()
    sent_ebd = np.average(word_ebds, weights=weights, axis=0)
    return sent_ebd


def predict(records):
    for item in records:
        o_ebds = [sentence_embedding(line) for line in item['opt']]
        q_ebds = [sentence_embedding(line) for line in item['que']]
        A = np.float32(o_ebds)
        B = np.float32(q_ebds)

        dis = -cosine_similarity(A, B)

        # N, D = A.shape
        # M, D = B.shape
        # C1 = np.broadcast_to(np.expand_dims(A, 1), (N, M, D))
        # C2 = np.broadcast_to(np.expand_dims(B, 0), (N, M, D))
        # C = C1 - C2
        # dis = np.sum(C**2, axis=2)

        pred = np.argmin(dis.min(axis=1))
        item['pred'] = pred
    return records


with open('./data/valid.seg') as f:
    valid = json.load(f)
valid = predict(valid)
acc = np.mean(np.float32([item['pred'] == item['ans'] for item in valid]))
# pprint(valid[:10])
print(acc)
