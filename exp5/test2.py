import json
import numpy as np
import pandas as pd
from scipy.spatial.distance import cosine
from pprint import pprint
from itertools import chain
from tqdm import tqdm
from sklearn.metrics.pairwise import cosine_similarity

import util

wv = util.load_wv('./data/wv.pkl')

stop_words = '\n我他們的啦了…。，「」 '

def dis_mat(A, B):
    return -cosine_similarity(A, B)

def transform(lines):
    tokens = []
    tags = []
    for i, line in enumerate(lines):
        line = [x for x in line if x not in stop_words]
        ebds = util.tokens2ebd(wv, line, 75, mode='np')
        tokens.append(ebds)
        tags.extend([i] * len(ebds))
    return np.concatenate(tokens, axis=0), np.int32(tags)

def predict(records):
    for item in tqdm(records):
        A, tags = transform(item['opt'])
        B, _ = transform(item['que'])
        D = dis_mat(A, B)
        idx = np.argmin(D.min(axis=1))
        item['pred'] = tags[idx]
    return records

with open('./data/valid.seg') as f:
    valid = json.load(f)
valid = predict(valid)
acc = np.mean(np.float32([item['pred'] == item['ans'] for item in valid]))
# pprint(valid)
print(acc)

        


