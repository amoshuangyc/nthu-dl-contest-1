import json
import numpy as np
import pandas as pd
from scipy.spatial.distance import cosine
from sklearn.metrics.pairwise import cosine_similarity
from pprint import pprint

import util

wv = util.load_wv('./data/wv.pkl')

stop_words = '\n我他們的啦了…。，「」 '

def dis_mat(A, B):
    return -cosine_similarity(A, B)

def transform(tokens):
    return np.mean(util.tokens2ebd(wv, tokens, mode='np'), axis=0)

def predict(records):
    for item in records:
        A = [transform(tokens) for tokens in item['opt']]
        B = [transform(tokens) for tokens in item['que']]
        A = np.float32(A)
        B = np.float32(B)
        D = dis_mat(A, B)
        pred = np.argmin(D.min(axis=1))
        item['pred'] = pred
    return records

with open('./data/valid.seg') as f:
    valid = json.load(f)
valid = predict(valid)
acc = np.mean(np.float32([item['pred'] == item['ans'] for item in valid]))
print(acc)




