# Unsupervised Methods based on Word Embedding

## test1

- sentence embedding = average of word embeddings
- Matching at sentence level by cosine distance

valid: 0.52

## test2

- Matching at word level by cosine distance

valid: 0.44

## test3

- Matching at sentence level by wmdistance

valid: 0.42

## test4

- sentence embedding = weighted average of word embedding
- weights = tfidf
- Matching at sentence level by cosine distance

valid: 0.54

## test5

- sentence embedding = [this](https://openreview.net/pdf?id=SyK00v5xx)
- Matching at sentence level by cosine distance

valid: TODO

## test6

- gensim.doc2vec

valid: TODO
