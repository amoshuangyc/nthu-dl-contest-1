import json
import numpy as np
import pandas as pd
from tqdm import tqdm
from collections import Counter
from pprint import pprint

stop_words = '\n我他們的啦了…。，「」 '

def transform(s):
    for c in stop_words:
        s = s.replace(c, '')
    return Counter(s)

def distance(que, opt):
    cnt = que & opt
    return -sum(cnt.values())

def predict(data):
    for item in tqdm(data):
        pprint(item)

        que = transform(item['Question'])
        opts = [transform(item[f'Option{j}']) for j in range(6)]
        diss = [distance(que, opt) for opt in opts]
        pred = np.argmin(np.float32(diss))

        item['pred'] = pred
    return data

# with open('../raw/subset.json') as f:
#     valid = json.load(f)
# valid = predict(valid)
# acc = np.mean(np.float32([item['pred'] == item['answer'] for item in valid]))
# pprint(valid)
# print(acc)

df = pd.read_csv('../raw/Question.csv')
test = df.to_dict(orient='record')
test = predict(test)
pprint(test[:10])
df = pd.DataFrame()
df['ID'] = list(range(len(test)))
df['Answer'] = [item['pred'] for item in test]
df.to_csv('./naive1.csv', index=False)
print(len(df))