import json
import numpy as np
import pandas as pd
from tqdm import tqdm
from collections import Counter

with open('../raw/subset.json') as f:
    data = json.load(f)

def transform(s):
    s = s.replace('\n', '')
    return s

def distance(que, opt):
    return -len(opt)

cnt_correct = 0
for item in tqdm(data):
    que = transform(item['Question'])
    ans = item['answer']
    opts = [transform(item[f'Option{j}']) for j in range(6)]
    diss = [distance(que, opt) for opt in opts]
    pred = np.argmin(np.float32(diss))
    cnt_correct += (pred == ans)
print(cnt_correct / 50)

